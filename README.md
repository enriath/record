# record

An attempt at getting the ShareX screenshotting experience on Linux (and anything else that you can put Python on and
that also has FFMPEG)

The program will take a screenshot as soon as you call it, then display it on screen to crop to a specific area. If
taking a still image, it will use that full screenshot when cropping, so if it's on the preview, it will be in the final
image.

## Extra requirements
* Python 3.6 or higher (Lower versions might work, but the type hinting will break it)
* FFMPEG (probably doesn't matter what version)
* Pip for your Python install
* A GPU that supports VAAPI if you want to record video, might specifically need to be AMD with how I've set it up. 
GIFs and stills will work fine on anything.

## How to set up:

1. Clone the repo
2. Set up a virtual env in the clone directory
   ```bash
   python3 -m venv venv
   ```
3. Install requirements
   ```bash
   source venv/bin/activate
   pip3 install -r requirements.txt
   ```
Running `record.py` should now work.

## How to build/package:

1. Build with PyInstaller
   ```bash
   source venv/bin/activate
   python3 -m PyInstaller record.spec
   ```
2. Run `dist.sh`
    This will move the built executable into `~/.local/share/record` and symlink it into `~/.local/bin`
    
## How to use:
Once you have the code, built or not, you can run it by running the program like you would normally
```bash
# Without building
python3 record.py
# Built, but not in the PATH
./record
# Built and in the PATH
record
```
The program accepts the following 3 arguments:
* `still`: Takes a screenshot
* `video`: Takes a video
* `gif`: Takes a gif

While the preview is up, left click and drag to select the area to screenshot, right click to cancel.

By default, the program will use a temp directory in `/tmp`, and will put the screenshots in `~/Pictures/record`. These 
can both be configured in `constants.py`. I should probably make them look at env vars as well, but this is mainly a
personal-use project.

## Known issues:

* Performance on large video/gif captures is pretty terrible.
* The video option requires a specific set of cards that support VAAPI, without an option for software encoding.
* When the bottom of the preview is at the bottom of the screen, the controls and timer are not visible. 
    * Use the icon in the tray to end capture by left clicking it. Encoding will still happen in the background.
* XDG env vars are not respected.